# Advanced Git Workshop
Lab 04: Configuring some useful aliases

---

# Tasks

 - Create some useful aliases
 
 - Inspecting the current aliases
 
 - Deleting a wrong alias
 
 - Using aliases

---

## Create some useful aliases

 - How many times do you use the "checkout" command? Let's make it shorter:
 
```
$ git config --global alias.co checkout
```

 - Why not give simple names to elaborate commands?:
 
```
$ git config --global alias.undo "reset HEAD~1 --mixed"
```

 - Why not simplify the log command?:
 
```
$ git config --global alias.log-graph "log --graph --all  --decorate --oneline"
```

 - Tired of specifying the branches you're going to push? Let's give git that job:
 
```
$ git config --global alias.branch-name '!git rev-parse --abbrev-ref HEAD'
$ git config --global alias.publish '!git push -u origin $(git branch-name)'
```


 - You don't care much about maintaining order?:
 
```
$ git config --global alias.balagan '!git add -A && git commit -m "yahala balagan!" && git push'
```

---

## Inspecting the current aliases

 - Let's inspect your aliases:
 
```
$ git config --get-regexp alias
```

---

## Deleting a wrong alias

 - Our created git undo can be dangerous, let's remove it:
 
```
$ git config --global --unset alias.undo

```

 - Let's inspect your aliases again:
 
```
$ git config --get-regexp alias
```

---

## Using aliases

 - Let's clone a repository to work with:
 
```
$ git clone https://oauth2:gkLjhh5W4te-cJngRBxB@gitlab.com/sela-git-advanced-workshop/static-web-app.git lab4
$ cd lab4
```

 - Let's inspect it using our command "log-graph":
 
```
$ git log-graph
```

 - Let's checkout the branch "feature" using our alias:
 
```
$ git co feature
```

 - Finally let's do a change and use our balagan command:
 
```
$ echo stam > stam.txt
$ git balagan
```

 - The command will failed to push because the token used in the clone command only allows read
 
---

# Cleanup (optional)

 - Remove the repository used during the lab:
 
```
$ cd ..
$ rm -rf lab4
```
